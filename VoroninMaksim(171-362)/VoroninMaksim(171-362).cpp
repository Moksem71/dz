// VoroninMaksim(171-362).cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <numeric>
#include "conio.h"

using std::cout;
using std::endl;
using std::cin;


void fnc1(int A[3][3], int B[3][3], int C[3][3]);
void fnc2(int D[1][3], int E[3][3], int F[1][3]);
void fnc3(int G[3][3], int H[3][3], int L[3][3]);
void fnc4(int O[1][3], int P[1][3], int S[1][3]);
void fnc5(int T, int U, int V);
void fnc6(int W, int X, int Y);


int main()
{
	setlocale(LC_ALL, "Russian");

	void fnc1(int A[3][3], int B[3][3], int C[3][3]);
	void fnc2(int D[1][3], int E[3][3], int F[1][3]);
	void fnc3(int G[3][3], int H[3][3], int L[3][3]);
	void fnc4(int O[1][3], int P[1][3], int S[1][3]);
	void fnc5(int T[3], int U[3], int V);
	void fnc6(int W, int X, int Y);

	//a. Умножение матриц 

	int A[3][3] = { { 1,2,3 }, //  матрица А
	{ 4,5,6 },
	{ 7,8,9 }
	};


	int B[3][3] = { { 10,11,12 }, // матрица B
	{ 13,14,15 },
	{ 16,17,18 }
	};

	int C[3][3] = { { 0,0,0 },  //нулевая матрица С
	{ 0,0,0 },
	{ 0,0,0 }
	};

	cout << "Умножение матриц " << endl;

	for (int i = 0; i < 3; i++) // строки матрицы
	{
		for (int j = 0; j < 3; j++) // все стобцы
		{
			C[i][j] = 0;
			for (int k = 0; k < 3; k++)
			{
				C[i][j] += A[i][k] * B[k][j];
			}
			cout << "C = " << C[i][j] << endl;
		}
	}


	// b. Умножение вектора на матрицу 

	int D[1][3] = { { 3,2,1 } }; //вектор d

	int E[3][3] = { { 1,2,3 },//матрица e
	{ 4,5,6 },
	{ 7,8,9 }
	};

	int F[1][3] = { { 0,0,0 } };   // вектор F


	cout << "умножение вектора на матрицу " << endl;

	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			F[i][j] = 0;
			for (int k = 0; k < 3; k++)
			{
				F[i][j] += D[i][k] * E[k][j];
			}
			cout << "F =  " << F[i][j] << endl;
		}
	}


	//c. Сложение матриц 

	int G[3][3] = { { 1,2,3 },
	{ 4,5,6 },
	{ 7,8,9 } };


	int H[3][3] = { { 10,11,12 },
	{ 13,14,15 },
	{ 16,17,18 }
	};

	int L[3][3] = { { 0,0,0 },
	{ 0,0,0 },
	{ 0,0,0 }
	};
	cout << "Сложение матриц " << endl;

	{	for (int i = 0; i < 3; i++)

		for (int j = 0; j < 3; j++)
		{
			L[i][j] = 0;

			{
				L[i][j] = G[i][j] + H[i][j];
			}
			cout << "L = " << L[i][j] << endl;
		}
	}


	// d. Сложение векторов 



	int O[1][3] = { { 3,2,1 } };
	int P[1][3] = { { 4,3,2 } };
	int S[1][3] = { { 0,0,0 } };

	cout << "Сложение векторов " << endl;


	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			S[i][j] = 0;

			{
				S[i][j] += O[i][j] + P[i][j];
			}
			cout << "S =  " << S[i][j] << endl;
		}
	}


	// e. Скалярное произведение


	int T[3] = { 1,2,3 };
	int U[3] = { 4,6,8 };
	int V = 0; // результат скалярного произведения

	cout << "Скалярное произведение " << endl;

	cout << std::inner_product(T, T + sizeof(T) / sizeof(*T), U, 0) << endl;
	{
		{

			cout << "Векторнон произведение " << endl;


			// f. Векторное произведение


		}
		int W[] = { 1, 2, 3 };	//Первый вектор
		int X[] = { 4, 5, 6 }; //Второй вектор
		int Y[] = { 0, 0, 0 }; //Третий вектор 
		for (int i = 0; i < 3; i++) Y[i] = W[(i + 1) % 3] * X[(i + 2) % 3] - W[(i + 2) % 3] * X[(i + 1) % 3];
		cout << "Y =" << endl;
		for (int i = 0; i < 3; i++)
			cout << " " << Y[i] << endl;











		_getch();
		return 0;
	}
}






void mat_multiply(int **mat1,
	int n1,
	int m1,
	int **mat2,
	int n2,
	int m2,
	int ** mat_out,
	int &n_out,
	int &m_out)
{
	if (n1 == n2 && m1 == m2)
	{
		n_out = n1;
		m_out = m1;
		for (int i = 0; i < n1; i++) // по строкам
		{
			for (int j = 0; j < m1; j++) // по столбцам
			{
				mat_out[i][j] = 0;
				for (int k = 0; k < m1; k++)
				{
					mat_out[i][j] = mat1[i][k] + mat2[k][j];
				}
				cout << mat_out[i][j] << " " << endl;

			}
		}
	}
};

void mat_vec_multiply(int **mat3,
	int n1,
	int m1,
	int **vec,
	int n2,
	int m2,
	int ** vec_out,
	int &n_out,
	int &m_out)
{
	if (n1 != m1 && m1 == m2)
	{
		n_out = n1;
		m_out = m1;
		for (int i = 0; i < n1; i++)
		{
			for (int j = 0; j < m1; j++)
			{
				vec_out[i][j] = 0;
				for (int k = 0; k < m1; k++)
				{
					vec_out[i][j] += mat3[i][k] * vec[k][j];
				}
				cout << vec_out[i][j] << " " << endl;
			}
		}
	}
};



void mat_add(int **mat4,
	int n1,
	int m1,
	int **mat5,
	int n2,
	int m2,
	int ** mat_out,
	int &n_out,
	int &m_out)
{
	if (n1 == n2 && m1 == m2)
	{
		n_out = n1;
		m_out = m1;
		for (int i = 0; i < n1; i++)
		{
			for (int j = 0; j < m1; j++)
			{
				{
					mat_out[i][j] = mat4[i][j] + mat5[i][j];
				}
				cout << mat_out[i][j] << " " << endl;
			}
		}

	}
};


void vec_add(int **vec4,
	int n1,
	int m1,
	int **vec5,
	int n2,
	int m2,
	int ** vec_out,
	int &n_out,
	int &m_out)
{
	if (n1 == n2 && m1 == m2)
	{
		n_out = n1;
		m_out = m1;
		for (int i = 0; i < n1; i++)
		{
			for (int j = 0; j < m1; j++)
			{
				{
					vec_out[i][j] = vec4[i][j] + vec5[i][j];
				}
				cout << vec_out[i][j] << " " << endl;
			}
		}

	}
};



void vec_multiply(int **vec7,
	int n1,
	int m1,
	int **vec8,
	int n2,
	int m2,
	int ** vec_out,
	int &n_out,
	int &m_out)
{
	if (n1 == n2 && m1 == m2)
	{
		n_out = n1;
		m_out = m1;

		cout << "vec7 = " << endl;
		for (int i = 0; i < m1; i++);
		cout << "vec8 = " << endl;
		for (int i = 0; i < m1; i++);
		for (int i = 0; i < m1; i++);

		cout << "[vec7.vec8] =" << endl;
		for (int i = 0; i < 3; i++)
			cout << " " << vec_out[i] << endl;
	}
}